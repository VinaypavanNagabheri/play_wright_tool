import { expect, test } from '@playwright/test'  
      
      const baseUrl = "https://reqres.in/api"

      let number:number;

      let name="Vinay"

      let job="Automation"
      

     
     test.describe.parallel('API tests', () => {
    
       test('Single user assertion', async ({ request }) => {

       const response = await request.get(`${baseUrl}/users/2`)

       expect ( response.status()).toBe(200)
    
       })

      test('API test not valid end point', async ({ request }) => {
        const response = await request.get(`${baseUrl}/users/noendpoint`)

        expect ( response.status()).toBe(404)
      })
      

      test('Single user resouce', async ({ request }) => {

        const response = await request.get(`${baseUrl}/unknown/2`)

        expect ( response.status()).toBe(200)

        const responseValue = JSON.parse(await response.text())

        console.log(responseValue)
        
      })
      
      
test('Delete an user', async ({ request }) => {
    const _response=await request.delete(`${baseUrl}/users/2`);
  
    expect(_response.status()).toBe(204);
  
  })

  test('Get a userlist', async ({ request }) => {

    const _response=await request.get(`${baseUrl}/users?page=2`)
  
  
    expect(_response.status()).toBe(200);
  
    const res=await _response.json();
  
    console.log(res.data[0].last_name);
  
  })
  
      
test('Create a user', async ({ request }) => {

    const _response=await request.post(`${baseUrl}/users`, {

data: {

"name": name,

"job": job

 }

});

  



   expect(_response.status()).toBe(201);

   expect(_response.ok()).toBeTruthy();


   const res=await _response.json();
   expect(res.name).toEqual(name);

   expect(res.job).toEqual(job);

   number =res.name;

   console.log(number)
  
  })
  
  

  
})