import { expect, test } from '@playwright/test'

import AdactinClass from './Home.Pages'


    let adactin
    test('TC-101', async ({ page }) => {
       
        adactin = new AdactinClass(page)


        await adactin.navigate()

        await adactin.loginCredentials('testuser23', 'testpassword')
        await expect(page).toHaveTitle('Adactin.com - Search Hotel');
    })
    test('TC-102-To verify whether the check-out date field accepts a later date than check i date', async ({ page }) => {
        adactin = new AdactinClass(page)


        await adactin.navigate()
        await adactin.loginCredentials('testuser23', 'testpassword')
        await expect(page).toHaveTitle('Adactin.com - Search Hotel');

        const Today = new Date()
        let d1 = Today.toLocaleDateString().split('/')
        let month = d1[1]
        let year = d1[2]
        console.log(month)
        let d2 = Number(d1[0]) + 7;
        let checkIn = d2 + '/' + month + '/' + year
        console.log(d2 + '/' + month + '/' + year)
        let d3 = Number(d1[0]) + 5
        let checkOut = d3 + '/' + month + '/' + year
        console.log(checkOut)

        await adactin.location.selectOption('Sydney')
        await adactin.hotel.selectOption('Hotel Creek')
        await adactin.roomType.selectOption('Standard')
        
        await adactin.dateIn.fill(checkIn)
        await adactin.dateOut.fill(checkOut)
        await adactin.search.click();

        const tt = await page.locator("span[id='checkin_span']").textContent()
        await expect(tt).toContain('Check-In Date shall be before than Check-Out Date')
    })
    test('TC-103-To check if error is reported if check-out date field is past date', async ({ page }) => {
        adactin = new AdactinClass(page)
        await adactin.navigate()
        await adactin.loginCredentials('testuser23', 'testpassword')
        await expect(page).toHaveTitle('Adactin.com - Search Hotel');

         const now = new Date()
        let dateone =  now.toLocaleDateString().split('/')
        
        console.log("date-",dateone)
        let month = dateone[1]
        console.log("month-",month)
        let year = dateone[2]
        console.log("year-",year)
        let dateTwo = Number(dateone[0]) - 5;
        let checkIn = dateTwo + '/' + month + '/' + year
        console.log(dateTwo + '/' + month + '/' + year)
        let dateThree = Number(dateone[0]) - 3
        let checkOut = dateThree + '/' + month + '/' + year
        console.log(checkOut)

        await adactin.location.selectOption('Sydney')
        await adactin.hotel.selectOption('Hotel Creek')
        await adactin.roomType.selectOption('Standard')
        await adactin.dateIn.fill(checkIn)
        await adactin.dateOut.fill(checkOut)
        await adactin.search.click();

        const ee = await page.locator("span[id='checkin_span']").textContent();
        await expect(ee).toContain('Check-In Date shall be before than Check-Out Date')
        

    })

    test('TC-104 -location verification', async ({ page }) => {
        adactin = new AdactinClass(page)


        var location = 'Sydney';
        await adactin.navigate()
        await adactin.loginCredentials('testuser23', 'testpassword')
        await expect(page).toHaveTitle('Adactin.com - Search Hotel');

        const now = new Date()
        let dateOne = now.toLocaleDateString().split('/')
        let month = dateOne[0]
        let year = dateOne[2]
        console.log(month)
        let dateTwo = Number(dateOne[1]) + 1
        let checkOut = month + '/' + dateTwo + '/' + year
        console.log(checkOut)

        await adactin.location.selectOption(location)
        await adactin.hotel.selectOption('Hotel Creek')
        await adactin.roomType.selectOption('Standard')
        await adactin.dateOut.fill(checkOut)
        await adactin.search.click();

        const ad = await page.locator("//input[contains(@id,'location') and @type='text']")
        const a1 = await ad.getAttribute('value')
        const a2 = a1?.toString().trim()
        console.log('location' + a2 + 'location')
        console.log('location=' + location)
        expect(a1).toEqual(location)

        
        if (a1 === location) {
            console.log('success')
        }
        else {
            console.log('fail')
            test.fail
        }
    })

    test('TC-105-To verify whether Check-in date and Check-out date are being displayed in Select Hotel page', async ({ page }) => {
        adactin = new AdactinClass(page)

        var location = 'Sydney';

        await adactin.navigate()
        await adactin.loginCredentials('testuser23', 'testpassword')
        await expect(page).toHaveTitle('Adactin.com - Search Hotel');

        const now = new Date()
        let dateOne = now.toLocaleDateString().split('/')
        let month = dateOne[1]
        let year = dateOne[2]
        console.log(month)
        let dateTwo = Number(dateOne[0]) + 1
        let checkOut = dateTwo + '/' + '0' +month + '/' + year
        console.log(checkOut)
        
        await adactin.location.selectOption(location)
        await adactin.hotel.selectOption('Hotel Creek')
        await adactin.roomType.selectOption('Standard')
        await adactin.dateOut.fill(checkOut)
        await adactin.search.click();

        
        await expect(adactin.expectedCheckOutDt).toHaveValue(checkOut);

       
    })
    test('TC-106-Verify the no.of rooms', async ({ page }) => {
        adactin = new AdactinClass(page)

        var location = 'Sydney';
        const room = "1";
        await adactin.navigate()
        await adactin.loginCredentials('testuser23', 'testpassword')
        await expect(page).toHaveTitle('Adactin.com - Search Hotel');

        const now = new Date()
        let dateOne = now.toLocaleDateString().split('/')
        let month = dateOne[0]
        let year = dateOne[2]
        console.log(month)
        let d3 = Number(dateOne[1]) + 1
        let checkOut = month + '/' + d3 + '/' + year
        console.log(checkOut)

        await adactin.location.selectOption(location)
        await adactin.hotel.selectOption('Hotel Creek')
        await adactin.roomType.selectOption('Standard')
        await page.locator('#room_nos').selectOption('1')
        await adactin.dateOut.fill(checkOut)
        await adactin.search.click();

        
        const rooms = await page.locator("//input[contains(@name,'rooms') and @type='text']").getAttribute('value')
        await expect(rooms).toContain(room)

    })

